## Gradle release plugin

### Описание плагина
// TBD

### Как подключить плагин к проекту
1. В файл `settings.gradle` добавить репозиторий
```
pluginManagement {
    repositories {
        ...
        maven {
            url = "https://nexus.mobicode.pro/repository/public/"
        }
        ...
    }
}
```
2. В `build.gradle` добавить plugin `com.umbrellait.tools.gradle.release-plugin`
```
plugins {
    ...
    id "com.umbrellait.tools.gradle.release-plugin" version "1"
    ...
}
```
