package com.umbrellait.tools.gradle

import com.umbrellait.tools.gradle.model.Version
import org.apache.commons.configuration.PropertiesConfiguration
import org.eclipse.jgit.api.Git
import org.eclipse.jgit.storage.file.FileRepositoryBuilder
import org.gradle.api.Project
import java.io.File

internal object ReleaseUtil {

    fun incrementVersion(project: Project) {
        val repository = FileRepositoryBuilder()
            .setGitDir(File("${project.projectDir}/.git"))
            .readEnvironment()
            .findGitDir()
            .build()

        val git = Git(repository)
        var conf = PropertiesConfiguration("${project.projectDir}/gradle.properties")

        val currentVersion = Version.valueOf(conf.getString("version"))
        println("current version: $currentVersion")
        git.tag()
            .setName(currentVersion.toString())
            .call()

        val releaseBranch =
            if (currentVersion.isMajor) {
                git.branchCreate()
                    .setName("release/$currentVersion")
                    .call()
            } else {
                null
            }

        val nextVersion = increaseVersion(currentVersion)
        println("next version: $nextVersion")
        conf.setProperty("version", nextVersion)
        conf.save()

        git.add()
            .addFilepattern(".")
            .call()

        git.commit()
            .setMessage("Increase version $currentVersion -> $nextVersion")
            .call()

        // update version in new release branch
        releaseBranch?.run {
            git.checkout()
                .setName(name)
                .call()

            conf = PropertiesConfiguration("${project.projectDir}/gradle.properties")
            val version = Version.valueOf(conf.getString("version"))
            val patchVersion = patchVersion(version)
            println("next patch version: $patchVersion")
            conf.setProperty("version", patchVersion)
            conf.save()

            git.add().addFilepattern(".").call()
            git.commit()
                .setMessage("Increase version $version -> $patchVersion")
                .call()
        }

        git.close()
    }

    private fun increaseVersion(version: Version): Version =
        when {
            version.isMajor -> version.copy(major = version.major + 1)
            version.isMinor -> version.copy(minor = version.minor + 1)
            version.isHotfix -> version.copy(hotfix = version.hotfix + 1)
            else -> Version(0, 0, 0)
        }

    private fun patchVersion(version: Version): Version =
        when {
            version.isMajor -> version.copy(minor = 1)
            version.isMinor -> version.copy(hotfix = 1)
            version.isHotfix -> version.copy(hotfix = version.hotfix + 1)
            else -> Version(0, 0, 0)
        }
}