package com.umbrellait.tools.gradle

import org.gradle.api.Plugin
import org.gradle.api.Project

class ReleasePlugin : Plugin<Project> {

    override fun apply(project: Project) {

        project.tasks.register("incrementVersion") {
            it.group = "ci"
            it.doLast {
                ReleaseUtil.incrementVersion(project.rootProject)
            }
        }
    }
}