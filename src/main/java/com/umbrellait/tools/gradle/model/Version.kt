@file:Suppress("ReplaceGetOrSet")

package com.umbrellait.tools.gradle.model

internal data class Version(
    val major: Int,
    val minor: Int,
    val hotfix: Int
) {

    val isMajor
        get() = minor == 0 && hotfix == 0

    val isMinor
        get() = minor != 0 && hotfix == 0

    val isHotfix
        get() = hotfix != 0

    override fun toString(): String =
        when {
            isMajor -> "$major"
            isMinor -> "$major.$minor"
            isHotfix -> "$major.$minor.$hotfix"
            else -> ""
        }

    companion object {

        fun valueOf(verStr: String): Version {
            val parts = verStr.split(".")
            if (parts.isEmpty() || parts.size > 3) {
                throw IllegalArgumentException("Version should be in format XX.YY.ZZ")
            }
            val major: String = parts.get(0)
            val minor: String = parts.takeIf { it.size > 1 }?.get(1) ?: "0"
            val hotfix: String = parts.takeIf { it.size > 2 }?.get(2) ?: "0"
            if (major.toIntOrNull() == null || minor.toIntOrNull() == null || hotfix.toIntOrNull() == null) {
                throw IllegalArgumentException("Version should be in format XX.YY.ZZ")
            }
            return Version(
                major.toInt(),
                minor.toInt(),
                hotfix.toInt()
            )
        }
    }
}
