package com.umbrellait.tools.gradle

import com.umbrellait.tools.gradle.model.Version
import kotlin.math.pow

object ReleaseHelper {

    fun buildNumber(version: String) : Long =
        Version.valueOf(verStr = version).run {
            val result = major * 10.0.pow(4).toInt() + minor * 10.0.pow(2) + hotfix
            result.toLong()
        }
}